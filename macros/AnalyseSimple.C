#include <EmuEvent.h>
#include <EmuTrack.h>
#include <Riostream.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <fstream>

void AnalyseSimple(const char *infile = "emu.root") {

  TFile *f = TFile::Open(infile);
  if (!f) {
    Printf("Error: Couldn't open file '%s' !!!'", infile);
    return;
  }

  TTree *tree = (TTree*)f->Get("emuTree");
  EmuEvent *ev = new EmuEvent(0);
  tree->SetBranchAddress("EmuEvent", &ev);

  TH1D *histType = new TH1D("hType","Type distribution",7,0,7);
  TH1D *histTheta = new TH1D("hTheta","Theta distribution",180,0,180);
  TH1D *histTheta2 = new TH1D("hTheta","Theta distribution",180,0,180);
  TH1D *histN = new TH1D("hN","N tracks distribution",180,0,180);

  EmuTrack *t;
  for (Int_t i=0;i<tree->GetEntries();i++) {
    tree->GetEntry(i);
    Printf("Event %d",i);
    histN->Fill(ev->GetNTrack());
    // ev->Print("all");
    for(Int_t iTrack=0;iTrack<ev->GetNTrack();iTrack++) {
      t = ev->GetTrack(iTrack);
      Printf("Theta=%f",t->GetTheta());
      histType->Fill(t->GetType());
      if (t->GetType() == 3 ) {
        if (ev->GetNTrack()>40 && ev->GetNTrack()<60) {
          histTheta->Fill(t->GetTheta());
        }
        if (ev->GetNTrack()>0 && ev->GetNTrack()<40) {
          histTheta2->Fill(t->GetTheta());
        }
      }
    }
  }

  TCanvas *c = new TCanvas("c","Canvas");
  c->Divide(2,2);
  c->cd(1);
  histType->Draw();
  c->cd(2);
  histN->Draw();
  c->cd(3);
  histTheta->Draw();
  c->cd(4);
  histTheta2->Draw();
}
