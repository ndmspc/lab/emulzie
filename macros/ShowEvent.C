#include <EmuEvent.h>
#include <EmuTrack.h>
#include <Riostream.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <fstream>

void ShowEvent(Long64_t id=0, const char *opt = "all", const char *infile = "emu.root") {

  TFile *f = TFile::Open(infile);
  if (!f) {
    Printf("Error: Couldn't open file '%s' !!!'", infile);
    return;
  }

  TTree *tree = (TTree*)f->Get("emuTree");
  EmuEvent *ev = new EmuEvent(0);
  tree->SetBranchAddress("EmuEvent", &ev);
  tree->GetEntry(id);
  ev->Print(opt);
}
