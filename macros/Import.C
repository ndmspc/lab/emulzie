#include <EmuEvent.h>
#include <EmuTrack.h>
#include <Riostream.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <fstream>

// data_type: 0-exp, 1=mc1, ... N=mcN
void Import(Int_t data_type = 0, TString infile = "test.dat",
            TString output = "emu.root", Int_t idStart = 0, Int_t debug = 0) {

  if (output.IsNull()) {
    output = infile;
    output.ReplaceAll(".dat", ".root");
  }

  Long64_t nEvents = 0;
  Long64_t nLines = 0;

  TFile *fOut = TFile::Open(output.Data(), "RECREATE");
  if (!fOut) {
    Printf("Error: Couldn't open file '%s' !!!'", output.Data());
    return;
  }

  TTree *tree = new TTree("emuTree", "Emu Tree");
  EmuEvent *ev = new EmuEvent(0);
  tree->Branch("EmuEvent", &ev);

  EmuTrack *t;

  int a, b;
  Int_t id;
  Long64_t event_id;
  Double_t bimp;
  Int_t aTarget = 0;
  Int_t n[7];

  int type;
  Double_t theta, psi, z, px, py, pz, m, e;
  Int_t s1,s2,source=0;
  std::string line;
  std::ifstream f(infile.Data());
  while (std::getline(f, line)) {
      nLines++;
    if (debug > 1)
      Printf("header=%s", line.data());
    std::istringstream iss(line);

      if (data_type == 0 && (iss >> id >> event_id >> n[0] >> n[1] >> n[2] >> n[3] >> n[4] >> n[5] >> n[6])) { }
      else if (data_type == 1 && (iss >> id >> bimp >> s1 >> s2 >> n[0] >> n[1] >> n[2] >> n[3] >> n[4] >> n[5] >> n[6])) { }
      else if (data_type == 2 && (iss >> aTarget >> bimp >> s1 >> s2 >> n[0] >> n[1] >> n[2] >> n[3] >> n[4] >> n[5] >> n[6])) { }
      else if (data_type == 3 && (iss >> aTarget >> bimp >> n[0] >> n[1] >> n[2] >> n[3] >> n[4] >> n[5] >> s1 >> n[6])) { }
      else if (data_type == 4 && (iss >> id >> bimp >> n[0] >> n[1] >> n[2] >> n[3] >> n[4] >> n[5] >> n[6])) { }
      else if (data_type == 5 && (iss >> id >> bimp >> n[0] >> n[1] >> n[2] >> n[3] >> n[4] >> n[5] >> n[6])) { }
      else {
        Printf("nEvents=%lld aTarget=%d bimp=%f n[0]=%d n[1]=%d n[2]=%d n[3]=%d n[4]=%d n[5]=%d "
               "n[6]=%d s1=%d s2=%d",nEvents,
               aTarget, bimp, n[0], n[1], n[2], n[3], n[4], n[5], n[6],s1,s2);
        Printf("Error: nline=%lld", nLines);
        gSystem->Exec(TString::Format("vi %s +%lld", infile.Data(),nLines).Data());
        return;
      }
      if (data_type == 0 && debug > 0) {
        Printf("id=%d event_id=%lld n[0]=%d n[1]=%d n[2]=%d n[3]=%d n[4]=%d "
               "n[5]=%d "
               "n[6]=%d",
               id, event_id, n[0], n[1], n[2], n[3], n[4], n[5], n[6]);
      }
      if (data_type >0 && debug > 0) {
        Printf("id=%d bimp=%f n[0]=%d n[1]=%d n[2]=%d n[3]=%d n[4]=%d n[5]=%d "
               "n[6]=%d",
               id, bimp, n[0], n[1], n[2], n[3], n[4], n[5], n[6]);
      } 



    if (data_type > 0) {
      ev->SetID(idStart + nEvents);
      ev->SetBimp(bimp);
      ev->SetATarget(aTarget);
    } else {
      ev->SetID(event_id);
    }
    //Printf("================ %d", n[6]);
    for (Int_t i = 0; i < n[6]; i++) {

      std::getline(f, line);
      nLines++;
      if (debug > 1)
        Printf("track=%s", line.data());
      std::istringstream iss2(line);

      if ((data_type == 0 || data_type == 3) && (iss2 >> type >> theta >> psi >> z)) {
	if (debug > 0)
          Printf("type=%d theta=%f psi=%f z=%f", type, theta, psi, z);
      } else if ((data_type==1 || data_type==2) && (iss2 >> type >> theta >> psi >> z >> px >> py >> pz >> m >> e >> s1)) {
        if (debug > 0)
          Printf("type=%d theta=%f psi=%f z=%f px=%f py=%f pz=%f m=%f e=%f",
                 type, theta, psi, z, px, py, pz, m, e);
      } else if (data_type==4 && (iss2 >> type >> theta >> psi >> z >> px >> py >> pz >> m >> e )) {
        if (debug > 0)
          Printf("type=%d theta=%f psi=%f z=%f px=%f py=%f pz=%f m=%f e=%f",
                 type, theta, psi, z, px, py, pz, m, e);
      } else if (data_type==5 && (iss2 >> type >> theta >> psi >> z >> px >> py >> pz >> m >> e >> source)) {
        if (debug > 0)
          Printf("type=%d theta=%f psi=%f z=%f px=%f py=%f pz=%f m=%f e=%f source=%d",
                 type, theta, psi, z, px, py, pz, m, e, source);
      } else {
          break;
      }

      t = ev->AddTrack();
      t->SetType(type);
      t->SetTheta(theta);
      t->SetPsi(psi);
      t->SetZ(z);
      if (data_type>0) {
        t->SetPx(px);
        t->SetPy(py);
        t->SetPz(pz);

        t->SetM(m);
        t->SetE(e);
        t->SetSource(source);
      }
    }
    tree->Fill();
    ev->Clear();
    nEvents++;

//    if (nEvents>10) return;
  }

  Printf("Total events processed (data_type=%d) : %lld", data_type, nEvents);
  fOut->Write();
  fOut->Close();
}
