void Graph(const char *filename = "data_FM_fi2") {

  TGraphErrors *g = new TGraphErrors(filename);
  g->SetTitle("#varphi_{2} dependence on <ns>");
  g->GetXaxis()->SetTitle("<ns>");
  g->GetYaxis()->SetTitle("#varphi_{2}");
  g->GetYaxis()->SetRangeUser(0, 0.05);
  g->SetMarkerStyle(20);
  g->Draw("AP");

  TText *t = new TText();
  t->DrawText(135, 0.04, "O+Ag(Br)");
}
