/// \file Analyse.C
/// \brief Analysis macro of task selector
///

#include <EmuAnalysisSelector.h>
#include <TArrayS.h>
#include <TChain.h>

void Run(TChain *ch, EmuAnalysisSelector *sel, Long64_t nEvents = 0);
Double_t GetError(Double_t a, Double_t ea, Double_t b, Double_t eb, Double_t c, Double_t ec);
void AnalyseAll(const char *fname = "emu.root", Long64_t nEvents = 0) {

  TString out1 = "emu_first.root";
  TString out2 = "emu_second.root";

  TChain *ch = new TChain("emuTree");
  ch->AddFile(fname);

  EmuAnalysisSelector *sel = new EmuAnalysisSelector();

  sel->SetOutputFileName(out1);

  EmuTaskInput *tm = new EmuTaskInput("EmuTaskMgr", "Emu task manager");
  EmuTaskEta *tEta = new EmuTaskEta("EtaTask", "Eta Task");
  tEta->SetHistParams(200, -10, 10);

  // tEta->AddIndex(1);
  // tEta->AddIndex(2);
  // tEta->SetMinMaxNumOfTracks(9, 1000);
  // tEta->SetMinMaxNumOfTracks(2,8);

  tEta->AddCutType(3);
  // tEta->AddCutType(6);
  // tEta->SetMinNumberOfRelativistic(7);
  // tEta->SetMinNumberOfRelativistic(11);
  // tEta->SetMinNumberOfRelativistic(16);
  tEta->SetMinNumberOfRelativistic(210);

  tm->Add(tEta);
  sel->SetTaskManager(tm);

  Run(ch, sel, nEvents);

  TFile *f = TFile::Open(out1.Data());
  auto hEta = (TH1D *)f->Get("EmuTaskMgr/EtaTask/hEta");
  // hEta->Print();
  // Printf("entries=%.0f mean=%f", hEta->GetEntries(), hEta->GetMean());
  sel->SetOutputFileName(out2);
  tEta->SetEtaHist(hEta);
  Run(ch, sel, nEvents);

  TFile *f2 = TFile::Open(out2.Data());
  auto hZZ = (TH1D *)f2->Get("EmuTaskMgr/EtaTask/hZZ");
  auto hz = (TH1D *)f2->Get("EmuTaskMgr/EtaTask/hz");
  auto hzz = (TH1D *)f2->Get("EmuTaskMgr/EtaTask/hzz");
  auto hNtotal = (TH1D *)f2->Get("EmuTaskMgr/EtaTask/hNtotal");
  Printf("hEta entries=%.0f mean=%f std=%f", hEta->GetEntries(), hEta->GetMean(),hEta->GetStdDev());
  Printf("hZZ entries=%.0f mean=%f std=%f", hZZ->GetEntries(), hZZ->GetMean(),hZZ->GetStdDev());
  Printf("hz entries=%.0f mean=%f std=%f", hz->GetEntries(), hz->GetMean(), hz->GetStdDev());
  Printf("hzz entries=%.0f mean=%f std=%f", hzz->GetEntries(), hzz->GetMean(),hzz->GetStdDev());
  Printf("hNtotal entries=%.0f mean=%f std=%f", hNtotal->GetEntries(),
         hNtotal->GetMean(),hNtotal->GetStdDev());

  Double_t err = GetError(hZZ->GetMean(),hZZ->GetMeanError(),hNtotal->GetMean(),hNtotal->GetMeanError(), hz->GetStdDev(), hz->GetStdDevError());
  Printf("Phi(z)=%f +/- %f", TMath::Sqrt(hZZ->GetMean() / hNtotal->GetMean()) -
                       hz->GetStdDev(),err);
  Printf("Phi(zz)=%f", TMath::Sqrt(hZZ->GetMean() / hNtotal->GetMean()) -
                       TMath::Sqrt(hzz->GetMean() / hNtotal->GetEntries()));

  // hEta2->Print();
}

void Run(TChain *ch, EmuAnalysisSelector *sel, Long64_t nEvents) {

  if (nEvents)
    ch->Process(sel, "", nEvents, 0);
  else
    ch->Process(sel);
}

Double_t GetError(Double_t a, Double_t ea, Double_t b, Double_t eb, Double_t c, Double_t ec) {
  Printf("a=%f +/- %f", a, ea);
  Printf("b=%f +/- %f", b, eb);
  Printf("c=%f +/- %f", c, ec);
  return TMath::Sqrt(0.25*a/b*((ea*ea)/(a*a)+(eb*eb)/(b*b))+(ec*ec));
}
