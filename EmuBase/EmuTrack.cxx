#include "EmuTrack.h"
#include <TMath.h>
#include <TRandom.h>
#include <TString.h>

/// \cond CLASSIMP
ClassImp(EmuTrack);
/// \endcond

EmuTrack::EmuTrack()
    : TObject(), fType(0), fPx(0.0), fPy(0.0), fPz(0.0), fCharge(0), fZ(0),
      fM(0), fE(0), fTheta(0), fPsi(0) {
  ///
  /// A constructor
  ///

  gRandom->SetSeed(0);
}

EmuTrack::~EmuTrack() {
  ///
  /// A destructor
  ///
}

void EmuTrack::BuildRandom() {
  ///
  /// Building random event
  ///

  Double_t px, py;
  gRandom->Rannor(px, py);
  fPx = px;
  fPy = py;
  fPz = TMath::Sqrt(px * px + py * py);

  // Generate charge
  fCharge = (gRandom->Integer(2) > 0) ? 1 : -1;
}

void EmuTrack::Print(Option_t * /*option*/) const {
  ///
  /// Printing track info
  ///

  Printf("type=%d theta=%.3f psi=%.3f z=%.3f ch=%d px=%.3f py=%.3f pz=%.3f m=%.3f e=%.3f source=%d",
         fType, fTheta, fPsi, fZ, fCharge, fPx, fPy, fPz, fM, fE, fSource);
}

void EmuTrack::Clear(Option_t *) {
  ///
  /// Reseting track to default values
  ///

  fCharge = 0;
  fPx = 0;
  fPy = 0;
  fPz = 0;
}

void EmuTrack::SetP(Double_t *p) {
  ///
  /// Sets all components of momentum
  ///
  fPx = p[0];
  fPy = p[1];
  fPz = p[2];
}

Double_t EmuTrack::GetEta() {
  ///
  /// Return eta (pseudo rapidity)
  ///
  Double_t c = 6.2831853/360;
  Double_t eps = 1e-5;
  // if less 0 -> rapidity of beam (in this case 2.25)
  if (fTheta <eps) return 2.25;
  return -TMath::Log(TMath::Tan(c*fTheta/2));
}
