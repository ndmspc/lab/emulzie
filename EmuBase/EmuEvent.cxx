#include "EmuEvent.h"
#include <TRandom.h>
#include <TString.h>

/// \cond CLASSIMP
ClassImp(EmuEvent);
/// \endcond

EmuEvent::EmuEvent()
    : TObject(), fID(0), fBimp(0), fATarget(0), fVx(0.0), fVy(0.0), fVz(0.0),
      fNTracks(0), fTracks(0) {
  ///
  /// Default constructor
  ///
}

EmuEvent::EmuEvent(Long64_t id, Double_t vx, Double_t vy, Double_t vz)
    : TObject(), fID(id), fBimp(0), fATarget(0), fVx(vx), fVy(vy), fVz(vz),
      fNTracks(0), fTracks(0) {
  ///
  /// A constructor
  ///

  fTracks = new TClonesArray("EmuTrack");
  gRandom->SetSeed(0);
}

EmuEvent::~EmuEvent() {
  ///
  /// A destructor
  ///

  delete fTracks;
  fTracks = 0;
}

EmuTrack *EmuEvent::AddTrack() {
  ///
  /// Adds track to event
  ///
  return (EmuTrack *)fTracks->ConstructedAt(fNTracks++);
}
void EmuEvent::Print(Option_t *option) const {
  ///
  /// Printing event info
  ///
  Printf("id=%lld n=%d vx=%.3f vy=%.3f vz=%.3f ATarget=%d", fID, fTracks->GetEntries(),
         fVx, fVy, fVz, fATarget);
  Printf("n1=%lld n2=%lld n3=%lld n4=%lld n5=%lld n6=%lld", GetNTrack(1),
         GetNTrack(2), GetNTrack(3), GetNTrack(4), GetNTrack(5), GetNTrack(6));

  if (!fTracks)
    return;

  TString str(option);
  str.ToLower();
  if (str.Contains("all")) {
    EmuTrack *t;
    for (Int_t i = 0; i < fTracks->GetEntries(); i++) {
      t = (EmuTrack *)fTracks->At(i);
      t->Print();
    }
  }
}

void EmuEvent::Clear(Option_t *) {
  ///
  /// Reseting event to default values and clear all tracks
  ///
  fID = 0;
  fVx = 0;
  fVy = 0;
  fVz = 0;

  fNTracks = 0;
  fTracks->Clear("C");
}

void EmuEvent::BuildVertexRandom() {
  ///
  /// Builds random vertex
  ///

  fVx = gRandom->Gaus();
  fVy = gRandom->Gaus();
  fVz = gRandom->Gaus();
}

Long64_t EmuEvent::GetNTrack(Short_t type) const {
  ///
  /// Return number of tracks with type t
  ///

  Long64_t n = 0;
  EmuTrack *t;
  for (Int_t i = 0; i < fTracks->GetEntries(); i++) {
    t = (EmuTrack *)fTracks->At(i);
    if (t->GetType() == type)
      n++;
  }

  return n;
}
