#ifndef EmuTrack_cxx
#define EmuTrack_cxx

#include <TObject.h>

///
/// \class EmuTrack
///
/// \brief Track object
///	\author Martin Vala <mvala@cern.ch>
///

class EmuTrack : public TObject {

public:
  EmuTrack();
  virtual ~EmuTrack();

  ///! \fn Short_t GetType() const
  /// Type of track
  ///\return type
  /// \fn Double_t GetPx() const
  /// Momentum x component
  /// \return Px component
  ///! \fn Double_t GetPy() const
  /// Momentum y component
  /// \return Py component
  ///! \fn Double_t GetPz() const
  /// Momentum z component
  ///\return Pz component
  ///! \fn Short_t GetCharge() const
  /// Charge of track
  ///\return charge
  /// \fn Double_t GetZ() const
  /// Z
  /// \return Z
  ///! \fn Double_t GetM() const
  /// Mass
  /// \return Mass
  ///! \fn Double_t GetE() const
  /// Energy
  ///\return Energy
  ///! \fn Double_t GetTheta() const
  /// Theta
  /// \return Theta
  ///! \fn Double_t GetPsi() const
  /// Psi
  ///\return Psi
  ///! \fn Short_t GetSource() const
  /// Source
  ///\return Source

  ///! \fn void SetType(Short_t t)
  /// \param t Type
  /// Sets type of track
  ///! \fn void SetPx(Double_t px)
  /// \param px Momentum x component
  /// Sets x component of momentum
  ///! \fn void SetPy(Double_t py)
  /// \param py Momentum y component
  /// Sets y component of momentum
  ///! \fn void SetPz(Double_t pz)
  /// \param pz Momentum z component
  /// Sets z component of momentum
  ///! \fn void SetCharge(Short_t ch)
  /// \param ch Charge
  /// Sets charge
  ///! \fn void SetZ(Double_t z)
  /// \param z Z component
  /// Sets z component of Z
  ///! \fn void SetM(Double_t m)
  /// \param m Mass
  /// Sets mass
  ///! \fn void SetE(Double_t e)
  /// \param e Energy
  /// Sets energy
  ///! \fn void SetTheta(Double_t theta)
  /// \param theta Theta
  /// Sets theta
  ///! \fn void SetPsi(Double_t psi)
  /// \param psi Psi
  /// Sets Psi
  ///! \fn void SetPsi(Short_t source)
  /// \param source Source
  /// Sets Source


  Short_t GetType() const { return fType; }
  Double_t GetPx() const { return fPx; }
  Double_t GetPy() const { return fPy; }
  Double_t GetPz() const { return fPz; }
  Short_t GetCharge() const { return fCharge; }
  Double_t GetZ() const { return fZ; }
  Double_t GetM() const { return fM; }
  Double_t GetE() const { return fE; }
  Double_t GetTheta() const { return fTheta; }
  Double_t GetPsi() const { return fPsi; }
  Double_t GetEta();
  Short_t GetSource() const { return fSource; }

  void SetType(Short_t t) { fType = t; }
  void SetPx(Double_t px) { fPx = px; }
  void SetPy(Double_t py) { fPy = py; }
  void SetPz(Double_t pz) { fPz = pz; }
  void SetP(Double_t *p);
  void SetCharge(Short_t ch) { fCharge = ch; }
  void SetZ(Double_t z) { fZ = z; }
  void SetM(Double_t m) { fM = m; }
  void SetE(Double_t e) { fE = e; }
  void SetTheta(Double_t theta) { fTheta = theta; }
  void SetPsi(Double_t psi) { fPsi = psi; }
  void SetSource(Short_t source) { fSource = source; }

  virtual void Print(Option_t *option = "") const;
  virtual void Clear(Option_t *option = "");

  void BuildRandom();

private:
  Short_t fType;   ///< Type
  Double_t fPx;    ///< Momentum x
  Double_t fPy;    ///< Momentum y
  Double_t fPz;    ///< Momentum z
  Short_t fCharge; ///< Charge
  Double_t fZ;     ///< Z
  Double_t fM;     ///< Mass
  Double_t fE;     ///< Energy
  Double_t fTheta; ///< Theta
  Double_t fPsi;   ///< Psi
  Short_t fSource; ///< Source (0,1,2)

  // TODO
  /// Copy constructor
  EmuTrack(const EmuTrack &);            /// not implemented
  EmuTrack &operator=(const EmuTrack &); /// not implemented

  /// \cond CLASSIMP
  ClassDef(EmuTrack, 1);
  /// \endcond
};

#endif
