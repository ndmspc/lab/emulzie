#ifndef EmuEvent_cxx
#define EmuEvent_cxx

#include "EmuTrack.h"
#include <TClonesArray.h>
#include <TObject.h>

///
/// \class EmuEvent
///
/// \brief Event object
///	\author Martin Vala <mvala@cern.ch>
///

class EmuEvent : public TObject {

public:
  EmuEvent();
  EmuEvent(Long64_t id, Double_t vx = 0.0, Double_t vy = 0.0,
           Double_t vz = 0.0);
  virtual ~EmuEvent();

  /// \fn Long64_t GetID() const
  /// Event ID
  /// \return event ID
  ///
  /// \fn Double_t GetBimp() const
  /// Impact parameter of collision
  /// \return Bimp Impact parameter of collision
  ///
  /// \fn Int_t GetATarget() const
  /// A of target
  /// \return A of target
  ///
  /// \fn Double_t GetVx() const
  /// Vertex x component
  /// \return Vx component
  ///
  /// \fn Double_t GetVy() const
  /// Vertex y component
  /// \return Vy component
  ///
  /// \fn Double_t GetVz() const
  /// Vertex z component
  /// \return Vz component
  ///
  ///! \fn void SetBimp(Double_t bimp)
  ///
  /// \param bimp Impact parameter of collision
  ///
  /// Sets bimp Impact parameter of collision
  ///
  ///! \fn void SetATarget(Int_t a)
  ///
  /// \param a A of target
  ///
  /// Sets A of target
  ///
  /// \fn void SetID(Long64_t id)
  /// \param id Event ID
  ///
  /// Sets event ID
  ///
  /// \fn void SetVx(Double_t vx)
  /// \param vx Vertex x component
  ///
  /// Sets x component of vertex
  /// \fn void SetVy(Double_t vy)
  /// \param vy Vertex x component
  ///
  /// Sets y component of vertex
  ///
  /// \fn void SetVz(Double_t vz)
  /// \param vz Vertex x component
  ///
  /// Sets z component of vertex
  ///
  /// \fn Long64_t GetNTrack() const
  /// \return number of tracks
  ///
  /// \fn EmuTrack *GetTrack(Long64_t id)
  /// \param id Track ID
  /// \return Track with id

  Long64_t GetID() const { return fID; }
  Double_t GetBimp() const { return fATarget; }
  Int_t GetATarget() const { return fBimp; }
  Double_t GetVx() const { return fVx; }
  Double_t GetVy() const { return fVy; }
  Double_t GetVz() const { return fVz; }

  void SetID(Long64_t id) { fID = id; }
  void SetBimp(Double_t bimp) { fBimp = bimp; }
  void SetATarget(Int_t a) { fATarget = a; }
  void SetVx(Double_t vx) { fVx = vx; }
  void SetVy(Double_t vy) { fVy = vy; }
  void SetVz(Double_t vz) { fVz = vz; }

  Long64_t GetNTrack() const { return fNTracks; }
  Long64_t GetNTrack(Short_t type) const;
  EmuTrack *GetTrack(Long64_t id) { return (EmuTrack *)fTracks->At(id); }
  EmuTrack *AddTrack();

  virtual void Print(Option_t *option = "") const;
  virtual void Clear(Option_t *option = "");

  void BuildVertexRandom();

private:
  Long64_t fID;   ///< ID of event
  Double_t fBimp; ///< Impact parameter of collision
  Int_t fATarget; ///< A of target
  Double_t fVx;   ///< Vertex x
  Double_t fVy;   ///< Vertex y
  Double_t fVz;   ///< Vertex z
  Int_t fNTracks; ///< Number of tracks

  /// Array with all tracks
  TClonesArray *fTracks; //->

  // TODO
  /// Copy constructor
  EmuEvent(const EmuEvent &);            /// not implemented
  EmuEvent &operator=(const EmuEvent &); /// not implemented

  /// \cond CLASSIMP
  ClassDef(EmuEvent, 2);
  /// \endcond
};

#endif
