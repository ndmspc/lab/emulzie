#!/bin/bash

if [ -z "$1" ];then
    echo "./%0 <name>"
    exit 1
fi

cp -f EmuTaskExample.h EmuTask${1}.h
cp -f EmuTaskExample.cxx EmuTask${1}.cxx

sed -i -e 's/EmuTaskExample/EmuTask'$1'/g' EmuTask${1}.h
sed -i -e 's/EmuTaskExample/EmuTask'$1'/g' EmuTask${1}.cxx

echo "Files 'EmuTask${1}.h' and 'EmuTask${1}.cxx' were created."
