#include "EmuTaskExample.h"

/// \cond CLASSIMP
ClassImp(EmuTaskExample);
/// \endcond
EmuTaskExample::EmuTaskExample(const char *name, const char *title)
    : EmuTask(name, title) {
  ///
  /// Default constructor
  ///
}

EmuTaskExample::~EmuTaskExample() {
  ///
  /// Destructor
  ///
}

void EmuTaskExample::Exec(Option_t * /*option*/) {
  ///
  /// Main function to process resonance study
  ///
}
