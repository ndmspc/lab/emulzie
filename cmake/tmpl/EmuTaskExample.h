#ifndef EmuTaskExample_cxx
#define EmuTaskExample_cxx

#include <EmuTask.h>

///
/// \class EmuTaskExample
///
/// \brief Example task
///	\author Martin Vala <mvala@cern.ch>
///

class EmuTaskExample : public EmuTask {

public:
  EmuTaskExample(const char *name = "EmuTaskExample",
                const char *title = "EmuTaskExample title");
  virtual ~EmuTaskExample();

  virtual void Exec(Option_t *option);

private:
  /// \cond CLASSIMP
  ClassDef(EmuTaskExample, 1);
  /// \endcond
};

#endif
