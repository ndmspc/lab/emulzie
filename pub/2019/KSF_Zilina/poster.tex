\documentclass{tikzposter}
\usepackage[utf8]{inputenc}
% \usepackage[slovak]{babel}
% dnf install texlive-tikzposter texlive-xstring texlive-a0poster  texlive-latexindent

\title{
 \begin{minipage}{\textwidth}
   \centering
   Relativistic particle production in high energy
   \\
   \bigskip
   \mbox{ nuclear collisions with Ag(Br)}
 \end{minipage}
}

\institute{Faculty of Science, P.J. Šafárik University, Košice, Slovakia}
\author{M. Vaľa, S. Vokál and J. Vrláková}
% \titlegraphic{Logo}
\usetheme{Board}
\begin{document}

\maketitle[width=1.00\textwidth]

\block{INTRODUCTION}{
    The study of global and local multiplicities of particles produced in 
    heavy ion collisions is important for several reasons. They are a most useful tool for event 
    characterization, for instance with respect to the centrality of the collision. 
    Systematic studies of the variation with mass, energy, and impact parameter 
    can provide the basis for the understanding of reaction mechanism \cite{stachel1991}. 
    The fluctuations in particle production may be used to signal the formation 
    of a quark-gluon plasma in the early stage of heavy ion interactions \cite{wong1994}. 
}

\block{EXPERIMENT}{
    Experimental data samples were collected by EMU01 and Dubna collaborations. 
    Nuclear interactions for various primary nuclei at Dubna, BNL and CERN energies 
    have been obtained using horizontally exposed emulsion detector. 
    All charged particles of measured interactions were classified according 
    to the commonly accepted emulsion experiment terminology into groups. 
    The group of relativistic (shower) particles includes particles (with > 0.7) 
    produced in the interactions as well as those knocked-out from the target nucleus. 
    The polar ($\Theta$) and azimuthal ($\Psi$) emission angles of all tracks have been measured. 
    The value of pseudorapidity ($\eta = - ln[tan \frac{\Theta}{2}]$) has been calculated 
    for each relativistic particle. 

    \noindent
    \begin{minipage}{.3\textwidth}
    \centering
    \begin{tikzfigure}[The interaction of $^{208}Pb$ at $158$ $A$ $GeV/c$ in emulsion detector.]
        \includegraphics[width=0.50\textwidth]{images/Fig1.jpg}
    \end{tikzfigure}
    \end{minipage}%
    \begin{minipage}{.3\textwidth}
    \centering
    \begin{tikzfigure}[The interaction of $^{32}S$ at $4.5$ $A$ $GeV/c$ in emulsion detector.]
        \includegraphics[width=0.60\textwidth]{images/Fig2.jpg}
    \end{tikzfigure}
    \end{minipage}
    \begin{minipage}{.3\textwidth}
    \centering
    \begin{tikzfigure}[The pseudorapidity distributions of relativistic particles in $^{16}O$ induced interactions at $4.5-200$ $A$ $GeV/c$.]
        \includegraphics[width=0.40\textwidth]{images/Fig3.jpg}
    \end{tikzfigure} 
    \end{minipage}
}
\begin{columns}
    \column{0.4}

\block{FACTORIAL MOMENT METHOD}{
    To study the dependence of factorial moment $F_{q}$, where $q$ is the order of the moment, 
    as a function of the bin width $\delta \eta$ has been suggested in \cite{bialas1986,bialas1988}. 
    The intermittent behaviour should lead to a power law dependence ,$q > 0$, 
    $\delta \eta$ is the pseudorapidity interval of produced relativistic particles. 
    The horizontal factorial moment method has been used for our analysis \cite{blazek1997}. Experimental data 
    obtained by the same standard emulsion method using various primary 
    nuclei (O, Ne, Si, S, Au, Pb) at momenta $4.1-200$ $A$ $GeV/c$ have been analysed. 
    The results of analysis show an evidence for the presence of intermittent behaviour 
    in all experimental samples and have been published in our previous papers, for example \cite{adamovich2001,vrlakova2006,volak2009,vokal147}.
    The  dependences of  values of slopes ($ln <Fq> = \alpha_{q} + \phi_{q} . ln M$) 
    on the order of factorial moments q have been studied for groups with 
    different degree of centrality. The dependence of values of 
    slopes $\phi_{2}$ on the mean pseudorapidity density $\rho$ 
    of relativistic particles in logarithmic scale has been shown figure below 
    The values for the central O, Ne, Si, S, Au and Pb interactions with Ag(Br) targets are shown. 
    This clearly show an inverse dependence upon pseudorapidity density for light primary 
    nuclei (A=16-32) and for heavy (A=197-208) ones \cite{vokal147}. 
    Our results are consistent with previous chamber emulsion data and pellicle detectors 
    published in \cite{wilkes1992}.
    
    \noindent
    \begin{minipage}{.3\textwidth}
    \centering
    \begin{tikzfigure}
        % [The dependences of $\phi_{2}$ on the mean pseudorapidity density $\rho$ of produced relativistic particles (in logarithmic scale) in nuclear interactions with Ag(Br) targets.]
        \includegraphics[width=0.40\textwidth]{images/Fig5.jpg}
    \end{tikzfigure} 
    \end{minipage}
}

\column{0.6}

\block{EVENT-BY-EVENT ANALYSIS}{
    The method for measurement of fluctuations, which vanishes in the case of 
    independent particle emission from a single source, has been proposed in \cite{gazdinski1992}. 
    Event-by-event fluctuations of observables, which are defined as a sum of particle 
    kinematics variables (rapidity or transverse momentum) and the summation runs over 
    all produced particles of given event, can be studied by this method \cite{bhattacharyya2013}. 
    Quatntity $\Phi$ is defined
    $$ \Phi = \sqrt{\frac{\langle Z ^{2}\rangle }{N_{t}}} - \sqrt{\bar{z^{2}}}$$
    where $ \sqrt{ \bar {z^{2}}} $ - the square of the 2nd moment of the inclusive $z$ distribution, 
    the quantity $\Phi$ measures the event-by-event fluctuations. In case of study 
    of pseudorapidity distributions of produced particles we can define $z$ as
    $z=\eta_{i} -\bar{\eta}$, where $\eta_{i}$ is pseudorapidity of produced $i$-particle 
    in a given event. If the produced particles are independent each other, 
    there are no correlations among the particles and the $\Phi$ value vanishes \cite{gazdinski1992}, 
    the non-zero of $\Phi$ may attributed to the measure of correlations among produced particles
     \cite{bhattacharyya2013}.
    The $\Phi$ dependences for $O+Em$ induced interactions at $4.5$ $A$ $GeV/c$ have been studied. 
    We selected central interactions with Ag(Br) targets, and we studied the groups 
    of interactions with increasing number of relativistic particles ($Ns >25, 30, 35$).
    The $\Phi$ value for $O+CNO$ targets have been calculated also for three overlaping 
    multiplicity groups with $Ns > 6,10,15$ (See Fig.5). 
    The $\Phi$ values have been calculated for Au and Pb central interactions with Ag(Br) target 
    for groups with increasing degree of centrality. The results are presented in table below.
    The $\Phi$ value decrease with increasing of average multiplicity of relativistic 
    particles for both data samples.

    \noindent
    % \begin{minipage}{.3\textwidth}
    % \centering
    % \begin{tikzfigure}[The dependences of $ln <Fq>$ on $ln M$  for Au+Ag(Br) at $11.6$ $A$ $GeV/c$.]
    %     \includegraphics[width=0.70\textwidth]{images/Fig4.jpg}
    % \end{tikzfigure} 
    % \end{minipage}%
    \begin{minipage}{.3\textwidth}
        \centering
        \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for O interactions with Ag(Br) and CNO targets at $4.5$ $A$ $GeV/c$.]
            \includegraphics[width=0.50\textwidth]{images/Fig6.jpg}
        \end{tikzfigure} 
        \end{minipage}
    \begin{minipage}{.3\textwidth}
    \centering
        \begin{tabular}{|c|c|c|}
            \hline 
            \multicolumn{3}{|c|}{Au+Ag(Br), $11.6$ $A$ $GeV/c$}\tabularnewline
            \hline 
            $N_{s}\geq$ &  & $\Phi$\tabularnewline
            \hline 
            $100$ & $188.64$ & $1.796\pm0.110$\tabularnewline
            \hline 
            $200$ & $264.28$ & $1.232\pm0.128$\tabularnewline
            \hline 
            $350$ & $362.00$ & $0.688\pm0.129$\tabularnewline
            \hline 
            \multicolumn{3}{|c|}{Pb+Ag(Br), $158$ $A$ $GeV/c$}\tabularnewline
            \hline 
            $N_{s}\geq$ &  & $\Phi$\tabularnewline
            \hline 
            $350$ & $636.78$ & $1.148\pm0.161$\tabularnewline
            \hline 
            $700$ & $901.74$ & $0.878\pm0.127$\tabularnewline
            \hline 
            $1000$ & $1121.71$ & $0.499\pm0.244$\tabularnewline
            \hline 
        \end{tabular}
     \end{minipage}
}

\end{columns}

\begin{columns}
    \column{0.5}
    \block{CONCLUSIONS}{
        
    The study of produced relativistic particles in central 
        $^{16}O$, $^{20}Ne$, $^{28}Si$, $^{32}S$, $^{197}Au$ and $^{208}Pb$
        induced collisions with Ag(Br) targets at $4.1 - 200$ $A$ $GeV/c$ has been 
        done using scaled factorial moments. The results of analysis show an 
        evidence for nonstatistical fluctuations in all experimental data 
        samples in pseudorapidity phase space. The inverse dependences of $\phi_{2}$
        on pseudorapidity density $\rho$ have been obtained.
        
         A preliminary study of multiplicity and target dependence of pseudorapidity 
         fluctuations in terms of the quantity $\Phi$ has been made for $^{16}O$
         induced collisions with Ag(Br) and CNO targets in emulsion detector. 
         Search for the event-by-event fluctuations of observable $\Phi$, 
         and its dependence on the degree of centrality in heavy ion collisions for 
         $^{197}Au$ and $^{208}Pb$ induced interactions with Ag(Br) targets at $11.6$ and 
         $158$ $GeV/c$ has been performed.
    }
    \block{ACKNOWLEDGMENT}{
        This research was supported by the Ministry of Education, 
        Science, Research and Sport of the Slovak Republic (VEGA No. 1/0113/18).
    }

    \column{0.5}
    \block{}{
        \begin{thebibliography}{9}
            \bibitem{stachel1991} 
            J. Stachel, Nucl. Phys. A525(1991) 23c.
            \bibitem{wong1994} 
            Ch. Y. Wong, Introduction to High-Energy Heavy- Ion Collisions, World Scientific Publ., 1994, 265.
            \bibitem{bialas1986} 
            A Bialas, R. Peschanski,Nucl.Phys.B273 (1986)703.
            \bibitem{bialas1988} 
            A.Bialas, R.Peschanski, Nucl.Phys.B308 (1988)857.
            \bibitem{blazek1997} 
            M. Blažek, Int. J. of Mod. Phys.A12(1997)839.
            \bibitem{adamovich2001} 
            M.I.Adamovich et al., APH N.S., HIP13(2001) 213.
            \bibitem{vrlakova2006} 
            J.Vrláková et al., Acta Phys.Slov. 56(2006)83.
            \bibitem{volak2009} 
            S.Vokál, J.Vrláková, Preprint JINR, E1-2009-188, Dubna, 2009.
            \bibitem{vokal147} 
            S.Vokál, J.Vrláková, Proc. of Rel. Nucl. Phys.: from Hundreds of MeV to TeV, 12th Int. Workshop, June 16 - 20, 2014, Stará Lesná, p.147.
            \bibitem{vokal67} 
            S.Vokál, J.Vrláková, Proc. of Rel. Nucl. Phys.: from Hundreds of MeV to TeV, 11th Int. Workshop, June 17-23, 2012, Stará Lesná, p.62.
            \bibitem{wilkes1992} 
            R. J. Wilkes et al., Nucl. Phys. A544(1992)153c.
            \bibitem{gazdinski1992} 
            M.Gazdzicki,S.Mrowczynski,Z.Phys.C54(1992)127.
            \bibitem{bhattacharyya2013} 
            S.Bhattacharyya et al., Phys. Lett. B 726(2013)194.
        \end{thebibliography}
        }
    \end{columns}


\end{document}
