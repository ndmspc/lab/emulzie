\documentclass{tikzposter}
\usepackage[utf8]{inputenc}
% \usepackage[slovak]{babel}
% dnf install texlive-tikzposter texlive-xstring texlive-a0poster  texlive-latexindent

\title{
 \begin{minipage}{\textwidth}
   \centering
   Search for pseudorapidity fluctuations
   \\
   in high energy nuclear collisions
%    \bigskip
%    \mbox{}
 \end{minipage}
}

\institute{Faculty of Science, P.J. Šafárik University, Košice, Slovakia}
\author{ J. Vrláková, S. Vokál and M. Vaľa}
% \titlegraphic{Logo}
\usetheme{Board}
\begin{document}

\maketitle[width=1.00\textwidth]

\block{INTRODUCTION}{
    The existence of quark-gluon plasma (QGP) - a new phase of strongly interacting systems at high energy or density has been predicted in the framework of the quantum chromodynamics. The study of relativistic interactions has provided the oportunity to search for the signal of QGP. Large fluctuations of particle production have been expected in the transition from QGP to hadron phase \cite{vanhove1984, Gyulassy}. The study of particle production is a most useful tool for event characterization, for instance with respect to the centrality of the collision. Systematic studies of the variation with mass, energy, and impact parameter can provide the basis for the understanding of reaction mechanism \cite{stachel1991}.
}

\begin{columns}
    \column{0.7}

\block{EXPERIMENT}{
    Nuclear interactions for various primary nuclei at different energies have been obtained using horizontally exposed emulsion detector.
    Experimental data samples were collected by EMU01 and Dubna collaborations.
    % The interaction of  $^{32}S$ at $4.5$ $A$ $GeV/c$ in emulsion detector is shown in Fig.1.
    All charged particles of measured interactions were classified according
    to the commonly accepted emulsion experiment terminology into groups.
    The group of relativistic (shower) particles includes particles (with $\beta  > 0.7$)
    produced in the interactions as well as those knocked-out from the target nucleus.
    The polar ($\Theta$) and azimuthal ($\Psi$) emission angles of all tracks have been measured.
    The value of pseudorapidity ($\eta = - ln[tan \frac{\Theta}{2}]$) has been calculated
    for each relativistic particle. The pseudorapidity distributions of relativistic particles in $^{16}O$ induced interactions at $4.5-200$ $A$ $GeV/c$ is presented in Fig.\ref{Fig2}.
    The multiplicity of relativistic shower particles ($N_s$) has been used as a criterion for event centrality
     \cite{emu01_NP}. The dependence of the number of produced relativistic particles on the impact parameter calculated by modified
     FRITIOF model for $Au$ induced interactions in emulsion is shown in Fig.\ref{Fig3}. The interactions with $N_{s} > 100$ are those
     of gold nuclei with the heavy emulsion targets $Ag$ and $Br$ \cite{rnp2012}.

    \noindent
    \begin{minipage}{.2\textwidth}
    \centering
    \begin{tikzfigure}
        [The interaction of $^{32}S+Em$ at $4.5$ $A$ $GeV/c$.]
        \includegraphics[width=0.50\textwidth]{images/Fig1.jpg}
    \label{Fig1}
    \end{tikzfigure}
    \end{minipage}%
    \begin{minipage}{.2\textwidth}
    \centering
    \begin{tikzfigure}[The pseudorapidity distributions of relativistic particles in $^{16}O$ induced interactions at $4.5-200$ $A$ $GeV/c$.]
        \includegraphics[width=0.40\textwidth]{images/Fig2.jpg}
        \label{Fig2}
    \end{tikzfigure}
    \end{minipage}
    \begin{minipage}{.2\textwidth}
    \centering
    \begin{tikzfigure}[The dependence of $N_s$ particles on the impact parameter($b_{imp}$) for $Au+Em$ interactions at 11.6 A GeV/c calculated by the FRITIOF model.]
        \includegraphics[width=0.40\textwidth]{images/Fig3.jpg}
        \label{Fig3}
    \end{tikzfigure}
    \end{minipage}
}

    \column{0.3}

\block{METHOD OF ANALYSIS}{
   The method for measurement of fluctuations, which vanishes in the case of
    independent particle emission from a single source, has been proposed in \cite{gazdz1992}.
    Event-by-event fluctuations of observables, which are defined as a sum of particle
    kinematics variables (rapidity or transverse momentum) and the summation runs over
    all produced particles of given event, can be studied by this method \cite{bhattacharyya2013}.
    Quantity $\Phi$ is defined as $ \Phi = \sqrt{\frac{\langle Z ^{2}\rangle }{N_{t}}} - \sqrt{\bar{z^{2}}}$,
    where $ \sqrt{ \bar {z^{2}}} $ - the square of the 2nd moment of the inclusive $z$ distribution,
    the quantity $\Phi$ measures the event-by-event fluctuations. In case of study
    of pseudorapidity distributions of produced particles we can define $z$ as
    $z=\eta_{i} -\bar{\eta}$, where $\eta_{i}$ is pseudorapidity of produced $i$-particle
    in a given event. If the produced particles are independent each other,
    there are no correlations among the particles and the $\Phi$ value vanishes \cite{gazdz1992},
    the non-zero of $\Phi$ may attributed to the measure of correlations among produced particles
     \cite{bhattacharyya2013}.
}

\end{columns}

\begin{columns}
\column{0.6}

\block{ANALYSIS AND RESULTS}{

   The $\Phi$ dependences for $O+Em$ induced interactions at $4.5 - 200$ $A$ $GeV/c$ have been studied using method of  the quantity $\Phi$.
   Experimental and Cascade evaporation model (CEM) data samples have been compared.
    We selected central interactions with Ag(Br) targets, and we studied the groups
    of interactions with increasing number of relativistic particles $Ns$ - see Fig.\ref{Fig4} for $O+Ag(Br)$ interactions at $4.5$ and $200$
    $A$  $GeV/ c$. One can see that the  $\Phi$ values for experimental data samples are higher than the values for CEM data \cite{RNP2019}.
    The similar results have been published in \cite{bhattacharyya2013}, where FRITIOF model has been used for comparison.

    The analysis have been done for various primary nuclei ( $O,Ne,Si,S$ ) at
    $4.1-4.5$ $A$ $GeV/c$ on  $Ag(Br)$ (Fig.\ref{Fig5}) and $CNO$ (Fig.\ref{Fig6}) targets.
    The $\Phi$ values for $CNO$ targets have been calculated also for different
    multiplicity groups. The $\Phi $ values for $Ag(Br)$ targets are greater than
    that for light ($CNO$) targets for the same primary nuclei, i.e. with increasing mass of
    target increases the $\Phi$ value and the correlations among particles increase. The $\Phi $
    values are decreasing with increasing average number of relativistic particles for all data.
    This can be explained by the formation of several independent sources during multiparticle
    production  \cite{bhattacharyya2013}. The similar results have been published in \cite{bhattacharyya2013} , \cite{zilina2019}.

    The $\Phi$ values for various primary nuclei with the heavy emulsion Ag(Br) targets at
    different momenta are given in the Tab.1, where $p$ is the momenta of primary nucleus $A$ and $<N_s>$
    is average number of relativistic particles. For light primary nuclei (A=16-32) the $\Phi $ values
    increase with increasing momenta. For heavy primary nuclei (A=197, 208) the $\Phi $
    values decrease with increasing momenta, this could be caused by the different numbers of produced relativistic particles.

    \noindent
    % \begin{minipage}{.3\textwidth}
    % \centering
    % \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for $O+Ag(Br)$ interactions at $4.5$ and $200$$A$ $GeV/c$, experimental a CEM data samples.]
    %     \includegraphics[width=0.20\textwidth]{images/Fig4.jpg}
    % \end{tikzfigure}
    % \end{minipage}%
    % \begin{minipage}{.3\textwidth}
    % \centering
    % \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for $A+Ag(Br)$ interactions at $4.1$ - $4.5$ $A$ $GeV/c$.]
    %     \includegraphics[width=0.20\textwidth]{images/Fig5.jpg}
    % \end{tikzfigure}
    % \end{minipage}
    % \begin{minipage}{.3\textwidth}
    % \centering
    % \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for $A+CNO$ interactions at $4.1$ - $4.5$ $A$ $GeV/c$.]
    %     \includegraphics[width=0.20\textwidth]{images/Fig6.jpg}
    % \end{tikzfigure}
    % \end{minipage}



    \begin{minipage}{.25\textwidth}
    \centering
        \begin{tabular}{|c|c|c|c|}
            \hline
           p [A GeV/c] &A&$<N_s>$&$\Phi$\tabularnewline
            \hline
        4.5  & $^{16}O$ & 30& 0.765 $\pm$ 0.066\tabularnewline
            \hline
            4.1  & $^{22}Ne$ & 33& 0.696 $\pm$ 0.043\tabularnewline
            \hline
            4.5  & $^{28}Si$ & 43& 0.859 $\pm$ 0.123\tabularnewline
            \hline
            4.5  & $^{32}S$ & 46& 0.689 $\pm$ 0.084\tabularnewline
            \hline
        14.6  & $^{16}O$ &65  & 0.618 $\pm$ 0.108\tabularnewline
            \hline
            14.6  & $^{28}Si$ &  93 & 0.899 $\pm$ 0.100\tabularnewline
            \hline
            11.6  & $^{197}Au$ & 189& 1.796 $\pm$ 0.110\tabularnewline
            \hline
        200  & $^{16}O$ &180  & 1.805 $\pm$ 0.170\tabularnewline
            \hline
            200  & $^{32}S$ &  259 & 1.833 $\pm$ 0.135\tabularnewline
            \hline
            158  & $^{208}Pb$ & 637& 1.148 $\pm$ 0.161\tabularnewline
            \hline
        \end{tabular}
     \end{minipage}
     \begin{minipage}{.3\textwidth}
     \centering
        \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for $O+Ag(Br)$ interactions at $4.5$ and $200$$A$ $GeV/c$, experimental a CEM data samples.]
            \includegraphics[width=0.80\textwidth]{images/Fig4.pdf}
            \label{Fig4}
        \end{tikzfigure}
    \end{minipage}%

}

\column{0.4}

\block{}{
    % \noindent

    \begin{minipage}{.3\textwidth}
    \centering
    \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for $A+Ag(Br)$ interactions at $4.1$ - $4.5$ $A$ $GeV/c$.]
        \includegraphics[width=0.90\textwidth]{images/Fig5.pdf}
        \label{Fig5}
    \end{tikzfigure}
    \end{minipage}
    \begin{minipage}{.3\textwidth}
    \centering
    \begin{tikzfigure}[The dependences of $\Phi$ values on the average multiplicity  for $A+CNO$ interactions at $4.1$ - $4.5$ $A$ $GeV/c$.]
        \includegraphics[width=0.90\textwidth]{images/Fig6.pdf}
        \label{Fig6}
    \end{tikzfigure}
    \end{minipage}

}

\block{ACKNOWLEDGMENT}{
    This research was supported by the Ministry of Education,
    Science, Research and Sport of the Slovak Republic (VEGA No. 1/0113/18).
}

\end{columns}

\begin{columns}
    \column{0.5}
    \block{CONCLUSIONS}{
         A preliminary study of multiplicity and target dependence of pseudorapidity
         fluctuations in terms of the quantity $\Phi$ has been made for  various primary nuclei
         with Ag(Br) and CNO targets in emulsion detector.
         Search for the event-by-event fluctuations of observable $\Phi$,
         and its dependence on the mass of primary nuclei, their momenta, different groups of events and model comparison has been performed.
         For all interactions and groups of events, the $\Phi$ values are found to be greater than zero and this indicates the presence of stronge correlations in the multiparticle production at momenta from $4.1$ to $200$  $A$ $GeV/c$.
         The experimental results have been compared with the results obtained from analysis of CEM data sample.
         The  $\Phi$ values for experimental data samples are higher than the values for CEM data.
         The $\Phi $ values for heavy $Ag(Br)$ targets are greater than that for light ($CNO$) targets for the same primary nuclei and momenta, i.e. with increasing target mass the correlations among the final state particles also increase.

    }
    % \column{0.4}


    \column{0.5}
    \block{}{
        \begin{thebibliography}{9}
             \bibitem{vanhove1984}
             Van Hove L., Z. Phys. C21(1984)93.
             \bibitem{Gyulassy}
             Gyulassy M. et al., Nucl. Phys. B237(1984)477.
            \bibitem{stachel1991}
            J. Stachel, Nucl. Phys. A525(1991)23c.
           \bibitem{emu01_NP}
           Adamovich M.I. et al., Nucl. Phys. A593(1995)535.
        \bibitem{rnp2012}
       Vokál S. et al.,Proc.of RNP, 11th Int. Workshop,June 17-23,2012,Stará Lesná,p.62.
        \bibitem{gazdz1992}
            M.Gazdzicki,S.Mrowczynski,Z.Phys.C54(1992)127.
        \bibitem{bhattacharyya2013}
            S.Bhattacharyya et al., Phys. Lett. B 726(2013)194.
            \bibitem{RNP2019}
            S.Vokál et al.,Proc. of RNP,14th Int.Workshop,May 27-June 1,2019,Stará Lesná,115.
           \bibitem{zilina2019}
            M.Vaľa et al., Proc. of 24th Conf. of Slovak Phys.,Sept. 2-9,2019,Žilina,p.77.

        \end{thebibliography}
        }
    \end{columns}


\end{document}