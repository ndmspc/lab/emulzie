#ifndef EmuTaskNDistribution_cxx
#define EmuTaskNDistribution_cxx

#include "EmuEvent.h"
#include "EmuTask.h"

class TH2D;
class THnSparse;
class TArrayS;

///
/// \class EmuTaskNDistribution
///
/// \brief Resonance task
///	\author Martin Vala <mvala@cern.ch>
///

class EmuTaskNDistribution : public EmuTask {

public:
  EmuTaskNDistribution(const char *name = "EmuTaskNDistribution",
                       const char *title = "EmuTaskNDistribution title");
  virtual ~EmuTaskNDistribution();

  virtual void Init(Option_t *option);
  virtual void Exec(Option_t *option);
  virtual void Finish(Option_t *option);

  void SetHistParams(Int_t nbins, Double_t min, Double_t max);
  void SetMinMaxNumOfTracks(TArrayS *idx, Int_t min, Int_t max);

private:
  /// Current event
  EmuEvent *fEvent;              //!
  TH2D *fHist;                   ///< Eta histogram
  Int_t fHistNBins;              ///< Number of bins in eta hist
  Double_t fHistMin;             ///< Minimum in eta hist
  Double_t fHistMax;             ///< Maximum in eta hist
  TArrayS *fIndexesNumberTracks; ///< list of ids to be sumed
  Int_t fMinNumberTracks;        ///< Minimum number of tracks
  Int_t fMaxNumberTracks;        ///< Maximum number of tracks

  /// \cond CLASSIMP
  ClassDef(EmuTaskNDistribution, 1);
  /// \endcond
};

#endif
