#include "EmuTaskEta.h"
#include "EmuEvent.h"
#include "EmuTrack.h"
#include <TH1D.h>
#include <TMath.h>

/// \cond CLASSIMP
ClassImp(EmuTaskEta);
/// \endcond

EmuTaskEta::EmuTaskEta(const char *name, const char *title)
    : EmuTask(name, title), fEvent(0), fEta(0), fZZ(0), fzz(0), fNTotal(0),
      fHistNBins(100), fHistMin(0.), fHistMax(1.), fIndexesNumberTracks(0),
      fMinNumberTracks(kMaxInt), fMaxNumberTracks(0), fMinRelativistic(-1) {
  ///
  /// Default constructor
  ///
}

EmuTaskEta::~EmuTaskEta() {
  ///
  /// Destructor
  ///
}

void EmuTaskEta::Init(Option_t *option) {
  ///
  /// Initialize histograms for resonance study
  ///

  if (!fEta) {
    fEta = new TH1D("hEta", "Eta", fHistNBins, fHistMin, fHistMax);
    fOutput->Add(fEta);
  } else {
    fZZ = new TH1D("hZZ", "Z^2", 1000, 0, 10000);
    fOutput->Add(fZZ);
    fz = new TH1D("hz", "z", 1000, 0, 10000);
    fOutput->Add(fz);
    fzz = new TH1D("hzz", "z^2", 1000, 0, 10000);
    fOutput->Add(fzz);

    fNTotal = new TH1D("hNtotal", "nTotal", 1000, 0, 10000);
    fOutput->Add(fNTotal);
  }

  fEvent = 0;

  // Do init for sub tasks
  EmuTask::Init(option);
}

void EmuTaskEta::Exec(Option_t * /*option*/) {
  ///
  /// Main function to process eta study
  ///
  TH1D *eta = (TH1D *)fOutput->FindObject(fEta);

  if (!fEvent) {
    EmuTask *input = fParent;
    fEvent = (EmuEvent *)input->GetOutput()->At(0);
  }
  fEvent->Print();
  //  Printf("\t%s %p", GetName(), fEvent);

  if (fIndexesNumberTracks.size() > 0) {
    Int_t numSelectedTracks = 0;
    for (auto i : fIndexesNumberTracks) {
      numSelectedTracks += fEvent->GetNTrack(i);
    }

    if ((numSelectedTracks < fMinNumberTracks) ||
        (numSelectedTracks > fMaxNumberTracks))
      return;
  }

  Int_t sum = 0;
  for (auto ct : fCutTypes) {
    sum += fEvent->GetNTrack(ct);
  }

  if ((fMinRelativistic >= 0) && (sum < fMinRelativistic))
    return;

  Long64_t nTracks = fEvent->GetNTrack();
  EmuTrack *t;
  Double_t ZZ = 0.0;
  Double_t z = 0.0;
  Double_t zz = 0.0;
  bool foundType = false;

  for (Long64_t i = 0; i < nTracks; i++) {
    t = (EmuTrack *)fEvent->GetTrack(i);

    foundType = false;
    for (auto ct : fCutTypes) {
      if (ct == t->GetType()) {
        foundType = true;
      }
    }
    if (!foundType)
      continue;

    t->Print();
    if (eta) {
      fEta->Fill(t->GetEta());
    } else {
      ZZ += t->GetEta() - fEta->GetMean();
      z = t->GetEta() - fEta->GetMean();
      if (fz) {
        fz->Fill(z);
      }
      zz = TMath::Power(z, 2);
      if (fzz) {
        fzz->Fill(zz);
      }
      Printf("eta=%f mean=%f sumZZ=%f z=%f zz=%f", t->GetEta(), fEta->GetMean(),
             ZZ, z, zz);
    }
  }

  ZZ = TMath::Power(ZZ, 2);
  if (fZZ) {
    fZZ->Fill(ZZ);
    Printf("ZZ=%f", ZZ);
  }

  if (fNTotal) {
    fNTotal->Fill(sum);
  }
}

void EmuTaskEta::Finish(Option_t * /*option*/) {
  ///
  /// Finish
  ///
  Printf("EmuTaskEta::Finish name=%s", GetName());
  fOutput->Print();
}

void EmuTaskEta::SetHistParams(Int_t nbins, Double_t min, Double_t max) {
  ///
  /// Sets histogram parameters
  ///

  fHistNBins = nbins;
  fHistMin = min;
  fHistMax = max;
}

void EmuTaskEta::SetMinMaxNumOfTracks(Int_t min, Int_t max) {
  ///
  /// Sets min number of tracks for indexes idx
  ///

  fMinNumberTracks = min;
  fMaxNumberTracks = max;
}
