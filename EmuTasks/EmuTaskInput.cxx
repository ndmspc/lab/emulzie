#include "EmuTaskInput.h"

/// \cond CLASSIMP
ClassImp(EmuTaskInput);
/// \endcond
EmuTaskInput::EmuTaskInput(const char *name, const char *title)
    : EmuTask(name, title) {
  ///
  /// Default constructor
  ///
}

EmuTaskInput::~EmuTaskInput() {
  ///
  /// Destructor
  ///
}

void EmuTaskInput::Exec(Option_t * /*option*/) {
  ///
  /// Main function to process resonance study
  ///
}
