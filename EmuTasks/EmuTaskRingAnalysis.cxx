#include "EmuTaskRingAnalysis.h"
#include "EmuEvent.h"
#include "EmuTrack.h"
#include <TH1D.h>
#include <TMath.h>

/// \cond CLASSIMP
ClassImp(EmuTaskRingAnalysis);
/// \endcond

EmuTaskRingAnalysis::EmuTaskRingAnalysis(const char *name, const char *title)
    : EmuTask(name, title), fEvent(0) {
  ///
  /// Default constructor
  ///
}

EmuTaskRingAnalysis::~EmuTaskRingAnalysis() {
  ///
  /// Destructor
  ///
}

void EmuTaskRingAnalysis::Init(Option_t *option) {
  ///
  /// Initialize histograms for Ring Analysis study
  ///

  fEvent = 0;

  // Do init for sub tasks
  EmuTask::Init(option);
}

void EmuTaskRingAnalysis::Exec(Option_t * /*option*/) {
  ///
  /// Main function to process eta study
  ///

  if (!fEvent) {
    EmuTask *input = fParent;
    fEvent = (EmuEvent *)input->GetOutput()->At(0);
  }
  fEvent->Print();
  //  Printf("\t%s %p", GetName(), fEvent);

  Long64_t nTracks = fEvent->GetNTrack();
  EmuTrack *t;
  for (Long64_t i = 0; i < nTracks; i++) {
    t = (EmuTrack *)fEvent->GetTrack(i);

    t->Print();
  }
}

void EmuTaskRingAnalysis::Finish(Option_t * /*option*/) {
  ///
  /// Finish
  ///
  Printf("EmuTaskRingAnalysis::Finish name=%s", GetName());
  fOutput->Print();
}
