#if defined(__CINT__) || defined(__ROOTCLING__)

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EmuTask + ;
#pragma link C++ class EmuTaskInput + ;
#pragma link C++ class EmuTaskNDistribution + ;
#pragma link C++ class EmuTaskEta + ;
#pragma link C++ class EmuTaskRingAnalysis + ;
#pragma link C++ class EmuAnalysisSelector + ;
#endif
