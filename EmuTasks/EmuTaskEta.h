#ifndef EmuTaskEta_cxx
#define EmuTaskEta_cxx

#include "EmuEvent.h"
#include "EmuTask.h"
#include <vector>

class TH1D;
class THnSparse;
class TArrayS;

///
/// \class EmuTaskEta
///
/// \brief Resonance task
///	\author Martin Vala <mvala@cern.ch>
///

class EmuTaskEta : public EmuTask {

public:
  EmuTaskEta(const char *name = "EmuTaskEta",
             const char *title = "EmuTaskEta title");
  virtual ~EmuTaskEta();

  virtual void Init(Option_t *option);
  virtual void Exec(Option_t *option);
  virtual void Finish(Option_t *option);

  void SetHistParams(Int_t nbins, Double_t min, Double_t max);
  void SetMinMaxNumOfTracks(Int_t min, Int_t max);
  /// Add index to calculate num of tracks
  void AddIndex(Short_t t) { fIndexesNumberTracks.push_back(t); }

  /// Cut on track type
  void AddCutType(Short_t t) { fCutTypes.push_back(t); }

  /// Sets eta histogram
  void SetEtaHist(TH1D *eta) { fEta = eta; }

  /// Sets minimum of relativistics particles
  void SetMinNumberOfRelativistic(Int_t m) { fMinRelativistic = m; }

private:
  /// Current event
  EmuEvent *fEvent;  //!
  TH1D *fEta;        ///< Eta histogram
  TH1D *fZZ;         ///< Z^2
  TH1D *fz;          ///< z
  TH1D *fzz;         ///< z^2
  TH1D *fNTotal;     ///< Ntotal
  Int_t fHistNBins;  ///< Number of bins in eta hist
  Double_t fHistMin; ///< Minimum in eta hist
  Double_t fHistMax; ///< Maximum in eta hist
  // TArrayS *fIndexesNumberTracks;    ///< list of ids to be sumed
  std::vector<Int_t> fIndexesNumberTracks{}; ///< list of ids to be sumed
  Int_t fMinNumberTracks;                    ///< Minimum number of tracks
  Int_t fMaxNumberTracks;                    ///< Maximum number of tracks
  std::vector<Short_t> fCutTypes{};          ///< List of cut types
  Int_t fMinRelativistic; ///< Minimum of relativistic particles

  /// \cond CLASSIMP
  ClassDef(EmuTaskEta, 1);
  /// \endcond
};

#endif
