#ifndef EmuTaskInput_cxx
#define EmuTaskInput_cxx

#include <EmuTask.h>

///
/// \class EmuTaskInput
///
/// \brief Input task
///	\author Martin Vala <mvala@cern.ch>
///

class EmuTaskInput : public EmuTask {

public:
  EmuTaskInput(const char *name = "EmuTaskInput",
              const char *title = "EmuTaskInput title");
  virtual ~EmuTaskInput();

  virtual void Exec(Option_t *option);

private:
  /// \cond CLASSIMP
  ClassDef(EmuTaskInput, 1);
  /// \endcond
};

#endif
