#include "EmuEvent.h"
#include "EmuTask.h"
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>

#include "EmuAnalysisSelector.h"

/// \cond CLASSIMP
ClassImp(EmuAnalysisSelector);
/// \endcond

EmuAnalysisSelector::EmuAnalysisSelector(TTree * /*tree*/)
    : TSelector(), fChain(0), fEvent(0), fTaskMgr(0),
      fOutputFileName("emu-out.root") {
  ///
  /// Default constructor
  ///
}

EmuAnalysisSelector::~EmuAnalysisSelector() {
  ///
  /// Destructor
  ///
  SafeDelete(fEvent);
}

void EmuAnalysisSelector::Init(TTree *tree) {
  ///
  /// Intialize tree
  ///
  if (!tree)
    return;
  fChain = tree;
  if (!fEvent) {
    fEvent = new EmuEvent(0);

    fChain->SetBranchAddress("EmuEvent", &fEvent);
    if (fTaskMgr)
      fTaskMgr->GetOutput()->Add(fEvent);
  }
}

Bool_t EmuAnalysisSelector::Notify() {
  ///
  /// Notify is called when file is changed
  ///
  if (fChain->GetCurrentFile())
    Printf("File : %s", fChain->GetCurrentFile()->GetName());
  return kTRUE;
}

void EmuAnalysisSelector::Begin(TTree * /*tree*/) {
  ///
  /// First user Function called on client
  ///
}

void EmuAnalysisSelector::SlaveBegin(TTree * /*tree*/) {
  ///
  /// First user Function called on proof worker
  ///

  TString option = GetOption();

  if (fTaskMgr)
    fTaskMgr->Init("");
}

Bool_t EmuAnalysisSelector::Process(Long64_t entry) {
  ///
  /// Process event with ID=entry
  ///

  GetEntry(entry);

  // if (entry % 1000 == 0)
  Printf("Event %lld ID=%lld nTracks=%lld", entry, fEvent->GetID(),
         fEvent->GetNTrack());

  if (fTaskMgr)
    fTaskMgr->ExecuteTask("");

  return kTRUE;
}

void EmuAnalysisSelector::SlaveTerminate() {
  ///
  /// Last user Function called on proof worker
  ///
  if (fTaskMgr)
    fTaskMgr->Finish("");
}

void EmuAnalysisSelector::Terminate() {
  ///
  /// Last user Function called on client
  ///

  TFile *f = TFile::Open(fOutputFileName.Data(), "RECREATE");
  fTaskMgr->Export(f);
  f->Close();
}

Int_t EmuAnalysisSelector::GetEntry(Long64_t entry, Int_t getall) {
  ///
  /// Gets entry
  ///
  return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0;
}
