#include "EmuTaskNDistribution.h"
#include "EmuEvent.h"
#include "EmuTrack.h"
#include <TH2D.h>

/// \cond CLASSIMP
ClassImp(EmuTaskNDistribution);
/// \endcond

EmuTaskNDistribution::EmuTaskNDistribution(const char *name, const char *title)
    : EmuTask(name, title), fEvent(0), fHist(0), fHistNBins(100), fHistMin(0.),
      fHistMax(1.), fIndexesNumberTracks(0), fMinNumberTracks(kMaxInt),
      fMaxNumberTracks(0) {
  ///
  /// Default constructor
  ///
}

EmuTaskNDistribution::~EmuTaskNDistribution() {
  ///
  /// Destructor
  ///
}

void EmuTaskNDistribution::Init(Option_t *option) {
  ///
  /// Initialize histograms for resonance study
  ///

  fHist =
      new TH2D("hN", "N Distribution", fHistNBins, fHistMin, fHistMax, 6, 0, 6);
  fOutput->Add(fHist);

  // Do init for sub tasks
  EmuTask::Init(option);
}

void EmuTaskNDistribution::Exec(Option_t * /*option*/) {
  ///
  /// Main function to process eta study
  ///

  if (!fEvent) {
    EmuTask *input = fParent;
    fEvent = (EmuEvent *)input->GetOutput()->At(0);
  }
  //  Printf("\t%s %p", GetName(), fEvent);
  Int_t numSelectedTracks = 0;
  if (fIndexesNumberTracks) {

    for (Int_t i = 0; i < fIndexesNumberTracks->GetSize(); i++) {
      numSelectedTracks += fEvent->GetNTrack(fIndexesNumberTracks->At(i));
    }
    if ((numSelectedTracks < fMinNumberTracks) ||
        (numSelectedTracks > fMaxNumberTracks))
      return;
  }

  Printf("numSelectedTracks=%d", numSelectedTracks);
  for (Short_t iType = 1; iType <= 6; iType++) {
    fHist->Fill(fEvent->GetNTrack(iType), (Double_t)iType - 0.1);
  }
}

void EmuTaskNDistribution::Finish(Option_t * /*option*/) {
  ///
  /// Finish
  ///
  Printf("EmuTaskNDistribution::Finish name=%s", GetName());
}

void EmuTaskNDistribution::SetHistParams(Int_t nbins, Double_t min,
                                         Double_t max) {
  ///
  /// Sets histogram parameters
  ///

  fHistNBins = nbins;
  fHistMin = min;
  fHistMax = max;
}

void EmuTaskNDistribution::SetMinMaxNumOfTracks(TArrayS *idx, Int_t min,
                                                Int_t max) {
  ///
  /// Sets min number of tracks for indexes idx
  ///

  fIndexesNumberTracks = idx;
  fMinNumberTracks = min;
  fMaxNumberTracks = max;
}
