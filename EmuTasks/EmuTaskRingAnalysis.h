#ifndef EmuTaskRingAnalysis_cxx
#define EmuTaskRingAnalysis_cxx

#include "EmuEvent.h"
#include "EmuTask.h"
#include <vector>

class TH1D;
class THnSparse;
class TArrayS;

///
/// \class EmuTaskRingAnalysis
///
/// \brief Resonance task
///	\author Martin Vala <mvala@cern.ch>
///

class EmuTaskRingAnalysis : public EmuTask {

public:
  EmuTaskRingAnalysis(const char *name = "EmuTaskRingAnalysis",
             const char *title = "EmuTaskRingAnalysis title");
  virtual ~EmuTaskRingAnalysis();

  virtual void Init(Option_t *option);
  virtual void Exec(Option_t *option);
  virtual void Finish(Option_t *option);

private:
  /// Current event
  EmuEvent *fEvent;  //!

  /// \cond CLASSIMP
  ClassDef(EmuTaskRingAnalysis, 1);
  /// \endcond
};

#endif
