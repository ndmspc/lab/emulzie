#ifndef EmuAnalysisSelector_H
#define EmuAnalysisSelector_H

#include <TSelector.h>

class TTask;
class TTree;
class EmuEvent;
class EmuTask;
///
/// \class EmuAnalysisSelector
///
/// \brief Task selector
///	\author Martin Vala <mvala@cern.ch>
///

class EmuAnalysisSelector : public TSelector {

public:
  EmuAnalysisSelector(TTree *tree = 0);
  virtual ~EmuAnalysisSelector();

  /// Returns selector version
  virtual Int_t Version() const { return 2; }
  virtual void Begin(TTree * /*tree*/);
  virtual void SlaveBegin(TTree *tree);
  virtual void Init(TTree *tree);
  virtual Bool_t Notify();
  virtual Bool_t Process(Long64_t entry);
  virtual Int_t GetEntry(Long64_t entry, Int_t getall = 0);
  virtual void SlaveTerminate();
  virtual void Terminate();

  /// \fn void SetTaskManager(TTask *tm)
  /// \param tm Task manager
  ///
  /// Sets task manager
  /// \fn void SetOutputFileName(const char *name)
  /// \param name output file name
  ///
  /// Sets output file name

  void SetTaskManager(EmuTask *tm) { fTaskMgr = tm; }
  void SetOutputFileName(const char *name) { fOutputFileName = name; }

private:
  /// Pointer to the analyzed TTree or TChain
  TTree *fChain; //!

  EmuEvent *fEvent;        ///< Current Event
  EmuTask *fTaskMgr;       ///< Task Manager
  TString fOutputFileName; ///< Output file

  /// \cond CLASSIMP
  ClassDef(EmuAnalysisSelector, 1);
  /// \endcond
};

#endif
